#!/usr/bin/env python

"""Make static website/blog with Python."""

import os
import shutil
import re
import glob
import sys
import json
import datetime
import commonmark
import logging

def read_file(filename):
    """Read file and close the file."""
    with open(filename, 'r') as f:
        return f.read()

def write_file(text, filename):
    """Write content to file and close the file."""
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    print(text,  file=open(filename, 'w'))

def truncate(text, words=25):
    """Remove tags and truncate text to the specified number of words."""
    return ' '.join(re.sub('(?s)<.*?>', ' ', text).split()[:words])

def read_headers(text):
    """Parse headers in text and yield (key, value, end-index) tuples."""
    for match in re.finditer(r'\s*<!--\s*(.+?)\s*:\s*(.+?)\s*-->\s*|.+', text):
        if not match.group(1):
            break
        yield match.group(1), match.group(2), match.end()

def rfc_2822_format(date_str):
    """Convert yyyy-mm-dd date string to RFC 2822 format date string."""
    d = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    return d.strftime('%a, %d %b %Y %H:%M:%S +0000')

def read_content(filename):
    """Read content and metadata from file into a dictionary."""
    # Read file content.
    text = read_file(filename)

    # Read metadata and save it in a dictionary.
    date_slug = os.path.basename(filename).split('.')[0]
    match = re.search(r'^(?:(\d\d\d\d-\d\d-\d\d)-)?(.+)$', date_slug)
    content = {
        'date': match.group(1) or '1970-01-01',
        'slug': match.group(2),
    }

    # Read headers.
    end = 0
    for key, val, end in read_headers(text):
        content[key] = val

    # Separate content from headers.
    text = text[end:]

    # Convert Markdown content to HTML.
    if filename.endswith(('.md', '.mkd', '.mkdn', '.mdown', '.markdown')):
        try:
            text = commonmark.commonmark(text)
        except ImportError as e:
            logging.error(f'WARNING: Cannot render Markdown in {filename}: {e}')

    # Update the dictionary with content and RFC 2822 date.
    content.update({
        'content': text,
        'rfc_2822_date': rfc_2822_format(content['date'])
    })

    return content

def render(template, **params):
    """Replace placeholders in template with values from params."""
    return re.sub(r'{{\s*([^}\s]+)\s*}}',
                  lambda match: str(params.get(match.group(1), match.group(0))),
                  template)

def make_pages(src, dst, layout, **params):
    """Generate pages from page content."""
    items = []

    for src_path in glob.glob(src):
        content = read_content(src_path)

        page_params = dict(params, **content)

        # Populate placeholders in content if content-rendering is enabled.
        if page_params.get('render') == 'yes':
            rendered_content = render(page_params['content'], **page_params)
            page_params['content'] = rendered_content
            content['content'] = rendered_content

        items.append(content)

        dst_path = render(dst, **page_params)
        output = render(layout, **page_params)

        logging.info(f'Rendering {src_path} => {dst_path} ...')
        write_file(output, dst_path)

    return sorted(items, key=lambda x: x['date'], reverse=True)

def make_list(posts, dst, list_layout, item_layout, **params):
    """Generate list page for a blog."""
    items = []
    for post in posts:
        item_params = dict(params, **post)
        item_params['summary'] = truncate(post['content'])
        item = render(item_layout, **item_params)
        items.append(item)

    params['content'] = ''.join(items)
    dst_path = render(dst, **params)
    output = render(list_layout, **params)

    logging.info(f'Rendering list => {dst_path} ...')
    write_file(output,dst_path)

def main():
    # Create a new public directory from scratch.
    if os.path.isdir('public'):
        shutil.rmtree('public')
    shutil.copytree('static', 'public')

    # Load parameter config
    params = json.loads(read_file('conf.json'))

    # Load layouts.
    page_layout = read_file('layout/page.html')
    post_layout = read_file('layout/post.html')
    list_layout = read_file('layout/list.html')
    item_layout = read_file('layout/item.html')

    # Combine layouts to form final layouts.
    post_layout = render(page_layout, content=post_layout)
    list_layout = render(page_layout, content=list_layout)

    # Create site pages.
    make_pages('content/_index.html', 'public/index.html',
               page_layout, **params)
    make_pages('content/[!_]*.html', 'public/{{ slug }}/index.html',
               page_layout, **params)

    # Create blogs.
    blog_posts = make_pages('content/blog/*.md',
                            'public/blog/{{ slug }}/index.html',
                            post_layout, blog='blog', **params)

    # Create blog list pages.
    make_list(blog_posts, 'public/blog/index.html',
              list_layout, item_layout, blog='blog', title='博客', **params)

if __name__ == '__main__':
    main()
