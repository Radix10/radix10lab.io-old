<!-- title: MySQL 34道练习题-->
# MySQL 34

建表语句与表数据
```sql
drop table if exists dept;     # 部门
drop table if exists salgrade; # 工资等级表
drop table if exists emp;      # 员工

create table dept (
        deptno int(10),
        dname varchar(14),
        loc varchar(13)
        );
create table salgrade (
        grade int(11),
        losal int(11),
        hisal int(11)
        );
create table emp (
        empno int(4) primary key,
        ename varchar(10),
        job varchar(9),
        mgr int(4),
        hiredate date,
        sal double(7,2),
        comm double(7,2),
        deptno int(2)
        );

insert into dept(deptno,dname,loc) values(10,'ACCOUNTING','NEW YORK');
insert into dept(deptno,dname,loc) values(20,'RESEARCHING','DALLAS');
insert into dept(deptno,dname,loc) values(30,'SALES','CHICAGO');
insert into dept(deptno,dname,loc) values(40,'OPERATIONS','BOSTON');
 
insert into salgrade(grade,losal,hisal) values(1,700,1200);
insert into salgrade(grade,losal,hisal) values(2,1201,1400);
insert into salgrade(grade,losal,hisal) values(3,1401,2000);
insert into salgrade(grade,losal,hisal) values(4,2001,3000);
insert into salgrade(grade,losal,hisal) values(5,3001,5000);
 
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7369,'SIMITH','CLERK',7902,'1980-12-17',800,null,20);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7499,'ALLEN','SALESMAN',7698,'1981-02-20',1600,300,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7521,'WARD','SALESMAN',7698,'1981-02-22',1250,500,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7566,'JONES','MANAGER',7839,'1981-04-02',2975,null,20);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7654,'MARTIN','SALESMAN',7698,'1981-09-28',1250,1400,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7698,'BLAKE','MANAGER',7839,'1981-05-01',2850,null,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7782,'CLARK','MANAGER',7839,'1981-06-09',2450,null,10);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7788,'SCOTT','ANALYST',7566,'1987-04-19',3000,null,20);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7839,'KING','PRESIDENT',null,'1981-11-17',5000,null,10);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7844,'TURNER','SALESMAN',7698,'1981-09-08',1500,null,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7876,'ADAMS','CLERK',7788,'1987-05-23',1100,null,20);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7900,'JAMES','CLERK',7698,'1981-12-03',950,null,30);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7902,'FORD','ANALYST',7566,'1981-12-03',3000,null,20);
insert into emp(empno,ename,job,mgr,hiredate,sal,comm,deptno)
	values(7934,'MILLER','CLERK',7782,'1982-01-23',1300,null,10);
```

## 1. 取得每个部门最高薪水的人员名称

a. 取得每个部门最高薪水
```sql
select e.deptno, max(e.sal) as maxsal
from emp as e
group by e.deptno;
```

b. 取得每个部门最高薪水的人员名称
```sql
select 
        e.deptno, e.ename, t.maxsal, e.sal
from (select 
        e.deptno, max(e.sal) as maxsal
      from 
        emp as e
      group by 
        e.deptno) as t
join 
        emp as e
on 
        t.deptno = e.deptno
where
        t.maxsal = e.sal
order by
        e.deptno;
```

## 2. 那些人的薪水在部门平均薪水之上

a. 首先找出部门的平均薪水
```sql
select 
        e.deptno, avg(e.sal) as salavg
from
        emp as e
group by
        e.deptno;
```

b. 然后查询那些人的薪水在部门平均薪水之上
```sql
select
        e.empno, e.sal, e.deptno
from
        emp as e
join
        (select 
                e.deptno, avg(e.sal) as salavg
        from
                emp as e
        group by
                e.deptno) as t
on 
        e.deptno = t.deptno
where
        e.sal > t.salavg
order by
        e.deptno;
```

## 3. 取得部门中所有人的平均的薪水等级

a. 首先获得所有人的工资等级
```sql
select
        e.deptno,s.grade
from
        emp as e
join
        salgrade as s
on e.sal between s.losal and s.hisal;
```

b. 按部门ID分组
```sql
select 
        t.deptno, avg(t.grade)
from (select
              e.deptno as deptno ,s.grade as grade
      from
              emp as e
      join
              salgrade as s
      on e.sal between s.losal and s.hisal) as t
group by t.deptno
```

## 4. 不准使用组函数Max，给出最高薪水（给出两种解决方案）

方式一：
```sql
select 
        sal 
from 
        emp 
order by 
        sal desc 
limit 1;
```

方式二：

```sql
select 
        e1.sal
from 
        emp as e1
join 
        emp as e2
on
        e1.sal < e2.sal;
```


```sql
select 
        sal        
from 
        emp
where
        sal not in (select 
                              e1.sal
                      from 
                              emp as e1
                      join 
                              emp as e2
                      on
                              e1.sal < e2.sal);
```

## 5. 取得平均薪水最高的部门和部门编号

```sql
select
        deptno, avg(sal) as salavg
from
        emp
group by
        deptno
order by 
        salavg desc
limit 1;
```

```sql
select
        deptno, avg(sal) as salavg
from
        emp
group by 
        deptno
having 
        salavg=(select
                        avg(sal) as salavg
                from
                        emp
                group by
                        deptno
                order by 
                        salavg desc
                limit 1);
```

## 6. 取得平均薪水最高的部门名称

```sql
select 
        t.deptno, d.dname, t.salavg
from
        (select 
                deptno, avg(sal) as salavg
        from
                emp as e
        group by
                e.deptno
        having
        salavg=(select
                        avg(sal) as salavg
                from
                        emp
                group by
                        deptno
                order by
                        salavg desc
                limit 1)) as t
join
        dept as d
on
        t.deptno = d.deptno;
```

## 7. 求平均薪水的等级最高的部门的部门名称

```sql
select
        d.dname, d.deptno, t.g
from
        (select
                t.deptno, s.grade as g, t.salavg
        from
                (select 
                        deptno, avg(sal) as salavg
                from
                        emp as e
                group by
                        e.deptno) as t
        join
                salgrade as s
        on
                t.salavg between s.losal and s.hisal) as t
join
        dept as d
on
        t.deptno = d.deptno
where
        t.g = (
                select 
                        max(t.g)
                from
                        (select
                                t.deptno, s.grade as g, t.salavg
                        from
                                (select 
                                        deptno, avg(sal) as salavg
                                from
                                        emp as e
                                group by
                                        e.deptno) as t
                        join
                                salgrade as s
                        on
                                t.salavg between s.losal and s.hisal) as t);

select 
        max(t.g)
from
        (select
                t.deptno, s.grade as g, t.salavg
        from
                (select 
                        deptno, avg(sal) as salavg
                from
                        emp as e
                group by
                        e.deptno) as t
        join
                salgrade as s
        on
                t.salavg between s.losal and s.hisal) as t
```



## 8. 取得比普通员工（员工代码没在mgr字段出现的）最高薪水更高的领导人姓名

```sql
select
        max(sal)
from
        emp
where 
        empno not in (select
                              distinct mgr
                      from
                              emp
                      where
                              mgr is not null);

select
        ename, sal
from
        emp
where 
        empno in (select
                          distinct mgr
                  from
                          emp
                  where
                          mgr is not null);


select
        ename, sal
from
        emp
where
        empno in (select
                          distinct mgr
                  from
                          emp
                  where
                          mgr is not null)
and
        sal > (select
                       max(sal)
               from
                       emp
               where 
                       empno not in (select
                                             distinct mgr
                                     from
                                             emp
                                     where
                                             mgr is not null));
```

## 9. 取得薪水最高的前五名员工

```sql
select
        deptno, sal
from
        emp
order by
        sal desc
limit
        5;
```
## 10. 取得薪水最高的第六到第十名员工

```sql
select
        *
from
        emp
order by
        sal desc
limit
        5, 5;
```
## 11. 取得最后入职的5名员工

```sql
select
        *
from
        emp
order by
        hiredate desc
limit
        5;
```
## 12. 取得每个薪水等级有多少员工

```sql
select
        t.grade, count(t.grade)
from (select
              *
      from
              emp as e
      join
              salgrade s
      on e.sal between s.losal and hisal) as t
group by
        t.grade

```

# 面试题
```sql
create table c(
	cno int(2) primary key auto_increment,
	cname varchar(32),
	cteacher varchar(16)
	);
	
create table s(
	sno int(2) primary key auto_increment,
	sname varchar(16)
	);

create table sc(
	sno int(2),
	cno int(2),
	scgrade int(3),
	primary key(sno,cno)
	);
	
insert into c(cname,cteacher)values('语文','张老师');
insert into c(cname,cteacher)values('政治','王老师');
insert into c(cname,cteacher)values('英语','李老师');
insert into c(cname,cteacher)values('数学','赵老师');
insert into c(cname,cteacher)values('物理','黎明');

insert into s(sname)values('学生1');
insert into s(sname)values('学生2');
insert into s(sname)values('学生3');
insert into s(sname)values('学生4');

insert into sc(sno,cno,scgrade)values(1,1,40);
insert into sc(sno,cno,scgrade)values(1,2,30);
insert into sc(sno,cno,scgrade)values(1,3,20);
insert into sc(sno,cno,scgrade)values(1,4,80);
insert into sc(sno,cno,scgrade)values(1,5,60);
insert into sc(sno,cno,scgrade)values(2,1,60);
insert into sc(sno,cno,scgrade)values(2,2,60);
insert into sc(sno,cno,scgrade)values(2,3,60);
insert into sc(sno,cno,scgrade)values(2,4,60);
insert into sc(sno,cno,scgrade)values(2,5,40);
insert into sc(sno,cno,scgrade)values(3,1,60);
insert into sc(sno,cno,scgrade)values(3,2,80);
```
## 1.显示没选”黎明“老师课的学生

```sql
黎明老师的课
select
        cno
from
        c
where
        cteacher = '黎明'

错误答案：
只过滤掉了包含黎明老师课程的单条记录，应该去除的是一个集合，但这里去除的是单挑的记录
没选”黎明“老师课的学生
select
        s.sno, s.sname
from
        s
join
        sc
on 
        s.sno = sc.sno
where
        sc.cno not in (select
                               cno
                       from
                               c
                       where
                               cteacher = '黎明');
+-----+-------+
| sno | sname |
+-----+-------+
| 1   | 学生1 |
| 1   | 学生1 |
| 1   | 学生1 |
| 1   | 学生1 |
| 2   | 学生2 |
| 2   | 学生2 |
| 2   | 学生2 |
| 2   | 学生2 |
| 3   | 学生3 |
| 3   | 学生3 |
+-----+-------+

错误答案：
sql中not in语句操作的集合不对，导致集错误。
select
        sno,sname
from
        s
where
        sno in (select
                        sno
                    from
                        sc
                    where
                        sc.cno not in (
                                select
                                        cno
                                from
                                        c
                                where
                                        cteacher = '黎明'));

+-----+-------+
| sno | sname |
+-----+-------+
| 1   | 学生1 |
| 2   | 学生2 |
| 3   | 学生3 |
+-----+-------+

正确答案
select
        sno,sname
from
        s
where
        sno not in (select
                        sno
                    from
                        sc
                    where
                        sc.cno in (
                                select
                                        cno
                                from
                                        c
                                where
                                        cteacher = '黎明'));

+-----+-------+
| sno | sname |
+-----+-------+
| 3   | 学生3 |
| 4   | 学生4 |
+-----+-------+
```

## 2.列出2门以上（含2门）不及格学生姓名及平均成绩
```sql
符合要求的学生的sno
select
        sno
from
        sc
where
        scgrade < 60
group by
        sno
having
        count(*) >= 2


符合要求学生的姓名
select
        sname
from
        s
where
        sno in (select
                        sno
                from
                        sc
                where
                        scgrade < 60
                group by
                        sno
                having
                        count(*) >= 2);

符合要求的学生及其平均成绩
select
        badst.sno, avgt.gavg
from
        (select
                 sno, avg(scgrade) gavg
         from
                 sc
         group by
                 sno) as avgt
join
        (select
                 sno
         from
                 sc
         where
                 scgrade < 60
         group by
                 sno
         having
                 count(*) >= 2) as badst
on
        badst.sno = avgt.sno
```

## 3. 学过1号课程和2号课程的所有学生的姓名
```sql
select
        sname
from
        s
where
        sno in (select
                        sno
                from
                        sc
                where
                        cno in (1, 2));

select
        sname
from
        s
where
        sno in (select
                        sno 
                from 
                        sc 
                where 
                        cno in (1,2) 
                group by 
                        sno);
```

## 14. 列出所有员工及领导的姓名
```sql
貌似连接的on条件顺序不是可以随意调换的。
select
        e.ename as ename, m.ename as mname
from
        emp as e
left join
        emp as m
on
        e.mgr = m.empno;

select 
        a.ename as empname,b.ename as leadname 
from 
        emp a 
left join 
        emp b 
on 
        a.mgr=b.empno;
```

## 15. 列出受雇日期早于其上级的所有员工的编号、姓名、部门名称

```sql
select
        e.empno, e.ename, d.dname
from
        (select
                 empno, ename, deptno
         from
                 emp
         where
         empno in (select
                           e.empno
                   from
                           emp e
                   left join
                           emp m
                   on
                           e.mgr = m.empno
                   where
                           e.hiredate < m.hiredate)) as e
join
        dept as d
on
        e.deptno = d.deptno

select
        e.empno, e.ename as ename, m.ename as mname, e.hiredate as ehiredate, m.hiredate as mhiredate
from
        emp e
join
        emp m
on
        e.mgr = m.empno
where
        e.hiredate < m.hiredate;
```
## 16, 列出部门名称和这些部门的员工信息，同时列出没有员工的部门
```sql
select
        d.dname, e.*
from
        dept as d
left join
        emp as e
on 
        d.deptno = e.deptno
```

## 17. 列出至少有5名员工的所有部门
```sql
select
        d.dname as dname, t.membercount as count
from
        (select
                 e.deptno as deptno, count(empno) as membercount
         from
                 emp as e
         group by
                 e.deptno
         having
                 count(empno) >= 5) as t
join
        dept as d
on
        t.deptno = d.deptno;
```

## 18. 列出薪水比simith多的所有员工信息
```sql
select
        *
from
        emp
where
        sal > (select
                       sal
               from
                       emp
               where
                       ename = 'simith');
```

## 19. 列出所有岗位为clerk的姓名及部门名称，部门人数
```sql
select
        *
from
        (select
                 d.deptno, count(e.ename), d.dname
         from
                 dept as d
         join
                 emp as e
         on
                 d.deptno = e.deptno
         group by
                 d.deptno, d.dname) as a
join
        (select
                 deptno, ename
         from
                 emp
         where
                 job = 'clerk') as b
on
        a.deptno = b.deptno;
```

## 20. 列出最低薪水大于1500的各种工作及从事此工作的全部雇员人数
```sql
select
        t.job, count(e.empno)
from
        emp as e
join
        (select
                 job, min(sal) as minsal
         from
                 emp
         group by
                 job
         having
                 minsal > 1500) as t
on
        e.job = t.job
group by
        t.job;
```

## 21. 列出在部门sales工作的员工的姓名，假定不知道销售部的部门编号
```sql
select
        ename
from
        emp as e
where
        e.deptno not in (select
                                 deptno
                         from
                                 dept
                         where
                                 dname != 'sales');
select
        *
from
        emp as e
join
        dept as d
on
        e.deptno = d.deptno
where
        d.dname = 'sales';

```

## 22. 列出薪水高于公司平均薪水的所有员工，所在部门，上级领导，薪水等级
```sql
select
        te.ename, te.sal, d.dname, te.ename, s.grade
from    (select
                 *
         from
                 emp
         where 
                 sal > (select
                                avg(sal)
                        from
                                emp)) as te
join
        dept as d
on
        te.deptno = d.deptno
join
        salgrade as s
on
        te.sal between s.losal and s.hisal;
```

## 23. 列出与scott从事相同工作的所有员工及部门名称
```sql
select
        e.ename, d.dname
from
        (select
                 *
         from
                 emp
         where
                 job = (select
                                job
                        from
                                emp
                        where
                                ename = 'scott')) as e
join
        dept as d
on
        e.deptno = d.deptno
where
        e.ename != 'scott';
```

## 24. 列出薪水等于部门30中员工的薪水的其他员工的姓名和薪水
```sql
select
        ename, sal
from
        emp
where
        sal in (select
                        sal
                from
                        emp
                where
                        deptno = 30)
and
        deptno != 30;
```

## 25. 列出薪水高于部门30的全部员工的员工姓名，薪水，部门名称
```sql
select
        e.ename, e.sal, d.dname
from
        (select
                 *
         from
                 emp as e
         where
                 sal > (select
                                sal
                        from
                                emp
                        where
                                deptno = 30
                        order by
                                sal desc
                        limit 1)) as e
join
        dept as d
on
        e.deptno = d.deptno;
```

## 26. 列出在每个部门工作的员工数量，平均工资和平均服务期限
```sql
select
        d.deptno, count(e.empno), ifnull(avg(e.sal),0),
        ifnull(avg((to_days(now()) - to_days(e.hiredate))/365),0) as avgtime
from
        emp as e
join
        dept as d
on
        e.deptno = d.deptno
group by
        deptno;
```

## 27. 列出所有员工的姓名、部门名称、工资
```sql
select
        e.ename, d.dname, e.sal
from
        emp as e
join
        dept as d
on 
        e.deptno = d.deptno
```

## 28. 列出所有部门的详细信息和人数
```sql
select
        d.deptno, count(e.empno)
from
        dept as d
join
        emp as e
on 
        d.deptno = e.deptno
group by
        d.deptno
```

## 29. 列出各种工作的最低工资以及从事此工作的雇员姓名
```
select
        e.ename, e.job
from
        emp as e
join
        (select
                 job, min(e.sal) as minsal
         from
                 emp as e
         group by
                 job) as t
on
        e.job = t.job
        and
        e.sal = t.minsal
select
        e.ename, e.job
from
        emp as e
join
        (select
                 job, min(e.sal) as minsal
         from
                 emp as e
         group by
                 job) as t
on
        e.job = t.job
where
        e.sal = t.minsal
```

## 30. 列出各个部门的manager的最低薪水
```sql

select
        e.ename, e.empno, t.minsal, e.deptno
from
        (select
                 deptno, min(sal) as minsal
         from
                 emp
         where
                 job = 'manager'
         group by 
                 deptno) as t
join
        emp as e
on 
        t.minsal = e.sal;
```

## 31. 列出员工的年工资，按年薪从低到高排序
```sql
select
        sal*12 as ysal
from
        emp
order by
        ysal desc
```

## 32. 求出员工领导的薪水超过3000的员工姓名和领导姓名

```sql
select
        e.ename
from
        emp as e
join
        emp as m
on
        e.mgr = m.empno
where
        m.sal > 3000
```

## 33. 求出部门名称中，带有‘s’字符的部门员工的工资合计，部门人数

```sql
select
        e.deptno, ifnull(sum(e.sal),0), ifnull(count(e.empno), 0)
from
        dept as d
left join
        emp as e
on 
        d.deptno = e.deptno
where
        d.dname like '%s%'
group by
        e.deptno
```

## 34. 给任职时间超过30年的员工加薪10%
```sql
update 
        emp 
set 
        sal=sal*1.1 
where 
        (to_days(now())-to_days(hiredate))/365>30;
```
