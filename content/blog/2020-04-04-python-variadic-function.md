<!-- title: Python中的可变参函数 -->
在Python中如果想要传递可变数量的参数，可以使用 `*args` 和 `**args` 来完成。

比如下面用来计算数字和的函数 `sum` 就可以使用 `*args` 来接收可变数量的参数。
```python
def sum(*params):
  sum = 0
  for i in params:
    sum += i
  return sum

print(sum(1, 2, 3, 4))
```

而接收命名可变参数可以使用 `**args` 来完成。

比如连接数据库时使用的 `connect` 函数。
```python
def connect(*args, **kwargs):

c = connect(user='scott', 
            password='password',
            host='127.0.0.1',
            database='employees')
```
