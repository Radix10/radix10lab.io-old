<!-- title: Python中的 __name__ 变量 -->
在Python脚本中经常可以见到如下脚本片段：

```python
if __name__ == "__main__"
  main()
```

其中 `__name__` 表示模块名，当此脚本直接执行时，其值被设置为 `__main__` 当被其他脚本导入时此值被设置为导入时的包名。
