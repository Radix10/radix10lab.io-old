<!-- title: 我想有个博客 -->
<!-- render: yes -->
<!-- weiboId: 网路冷眼 -->

我想有个博客，使用简洁优雅的工具生成。
而在考虑了Markdown、Asciidoc、Tex后感觉还是使用的Asciidoc用来写博客比较好。
但昨天看了好多静态网站生成工具，总感觉用它们来生成一个静态网站实在是太复杂臃肿了，且大多默认支持Markdown。

睡前无意间在微博看到了{{ weiboId }}发的一条有关静态博客生成工具的微博，了解到了sunainapai（GitHub用户名）的makesite.py，发现这正是我想要的。
